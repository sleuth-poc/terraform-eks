Welcome to this POC project. This small proof-of-concept project is used to demonstrate candidate's (Premdass) ability in GitOps based infrastructure-as-a-code in following technologies
* Kubernetes
* Terraform
* AWS and EKS

This terraform code creates a EKS cluster (v 1.17) with managed auto-scaling node group in a private VPC across 3 availability zones. It will 
also sets up a cluster-autoscaler to scale up the number of nodes if there are pending pods, and scales down if the nodes are under-utilized.
This code will also set-up nginx-ingress controller to serve up the external traffic in to the cluster using Network-load-balancer. 
This will add Linkerd as service mesh to the cluster, with Prometheus and Jaeger integrated for better monitoring and tracing. More details on how each component is setup
and configured is available below

[Note]: This is not a production ready code, but a POC code with limitations. This is only to show candidate can think through and set up a kubernetes cluster using
IaaC principles.


# Code structure
Terraform code is briefly organised in to env and modules. Env segregation enables us to have separate state files for the environments and 
slightly different configuration for each environment (e.g. test have 3 worker nodes while production has 10 worker nodes). Also, this enables 
us to apply the changes in each environment independently without affecting other environments. Modules provides a easier way to have a reusable 
code. Locked remote state is not used to keep things simple, however it is a must in a collaborative team environment.

Terraform commands need to be applied at the env/xxx level for everything to work. For example, to create a EKS cluster for dev environment, go inside env/dev and run terraform init and 
then terraform apply. GitOps can be setup by using either Terraform cloud or Jenkins/Atlantis for this repository .

# Network and VPC
A public subnet is created for the network loadbalancer, while private subnet is created for the worker nodes of the EKS cluster using 
    community terraform module `terraform-aws-modules/vpc/aws`. A NAT gateway is automatically setup(using route table) for internet access for private subnets , 
    and dns hostnames are enabled across vpc. Subnets are automatically tagged with the eks cluster name, and the environment
    
# EKS Cluster
Amazon's Elastic Kubernetes cluster is created by using terraform community module `terraform-aws-modules/eks/aws`. 
    This set-up a control plane for kubernetes, and an autoscaling node group for worker nodes. Cluster is setup to use the private subnet, 
    and also uses IRSA (IAM roles for service accounts). IRSA treats pods as first-class citizens of IAM, and let the cluster-autoscaler pod 
    scale the autoscaling worker group based on the cluster utilization.
    
# Cluster auto-scaler
Cluster autoscaler automatically adjusts the number of nodes in the auto-scaling node group based on the pod utilization. 
    Cluster autoscaler is installed in the cluster using Terraform Helm provisioner.
    
# Nginx Ingress
Ingress is an essential component in kubernetes architecture to allow the external traffic in to the cluster in a secure and scalable way . This
    architecture doesn't provision a new load balancer for each ingress entries. Instead a single Network load balancer is provisioned which sends the traffic
    to the nginx ingress controller, and the ingress controller handles all the routing and ingress related logic. This saves cost on the load balancer
    and also provides us an cloud-agnostic way to handle the traffic. Nginx ingress is installed in the cluster using terraform helm provider, while
    NLB is provisioned by using appropriate annotations on the nginx service created in the cluster
    
# Linkerd
Service mesh provides following benefits
    * Secure communication between pods
    * Better observability, monitoring and visibility
    * Traffic shifting to enable safer releases
    
This architecture uses Linkerd as a service mesh due to its simplicity. Linkerd is setup using Terraform helm provider . This also setups up Prometheus, grafana and Jaeger as addons
    in the cluster. Prometheus / Grafana provides monitoring and visibility of the communication and issues across the pod traffic, while Jaeger provides observability/performance of 
    api calls inside the pods.
    