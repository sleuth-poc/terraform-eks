output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks_cluster.cluster_eks.cluster_endpoint
}

output "cluster_id" {
  description = "Endpoint for EKS control plane."
  value       = module.eks_cluster.cluster_eks.cluster_id
}

