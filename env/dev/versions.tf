terraform {
  required_version = ">= 0.12"
}

provider "random" {
  version = "~> 2.2"
}