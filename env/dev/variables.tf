variable "env" {
  description = "Environment identifier to label the resources using env key"
  default     = "default"
}

variable "cidr_block" {
  description = "CIDR block for the VPC"
  default     = "0.0.0.0/0"
}


locals {
  cluster_name = "sleuth-${var.env}-${random_string.suffix.result}"
}