provider "aws" {
  region = "us-east-1"
}

resource "random_string" "suffix" {
  length  = 6
  special = false
  upper   = false
}


module "network" {
  source       = "../../modules/aws/network"
  cidr_block   = var.cidr_block
  env          = var.env
  cluster_name = local.cluster_name

}

module "security_group" {
  source       = "../../modules/aws/security-grp"
  cidr_block   = var.cidr_block
  env          = var.env
  cluster_name = local.cluster_name
  vpc_id       = module.network.vpc_id
}

module "eks_cluster" {
  source = "../../modules/aws/eks"

  cluster_name    = local.cluster_name
  env             = var.env
  worker_sec_grp  = module.security_group.worker_security_group_id
  cluster_subnets = module.network.private_subnets
  vpc_id          = module.network.vpc_id

}

data "aws_eks_cluster" "cluster" {
  name = module.eks_cluster.cluster_eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks_cluster.cluster_eks.cluster_id
}

provider "helm" {
  debug = true
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
    load_config_file       = false
  }
}

module "auto_scaler" {
  source     = "../../modules/aws/cluster-autoscaler"
  oidc_url   = module.eks_cluster.cluster_eks.cluster_oidc_issuer_url
  cluster_id = module.eks_cluster.cluster_eks.cluster_id
}

module "addons" {
  source         = "../../modules/kubernetes/addons"
  enable_ingress = true
  enable_linkerd = true
}
