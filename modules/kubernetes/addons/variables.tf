variable "enable_ingress" {
  description = " Boolean falg to enable ingress addon"
  default     = true
}

variable "enable_linkerd" {
  description = " Boolean falg to enable Linkerd addon"
  default     = true
}