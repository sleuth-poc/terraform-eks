module "ingress" {
  source = "./modules/ingress-nginx"

  enable_ingress = var.enable_ingress
}

module "linkerd" {
  source = "./modules/linkerd"

  enable_linkerd = var.enable_linkerd
}