#CA key
resource "tls_private_key" "trustanchors_key" {
  count = var.enable_linkerd ? 1 : 0

  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

# CA cert
resource "tls_self_signed_cert" "trustanchors_cert" {
  count = var.enable_linkerd ? 1 : 0

  key_algorithm         = tls_private_key.trustanchors_key[0].algorithm
  private_key_pem       = tls_private_key.trustanchors_key[0].private_key_pem
  validity_period_hours = 87600
  is_ca_certificate     = true

  subject {
    common_name = "identity.linkerd.cluster.local"
  }

  allowed_uses = [
    "crl_signing",
    "cert_signing",
    "server_auth",
    "client_auth"
  ]
}

# Issuer key
resource "tls_private_key" "issuer_key" {
  count = var.enable_linkerd ? 1 : 0

  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

# issuer CSR
resource "tls_cert_request" "issuer_req" {
  count = var.enable_linkerd ? 1 : 0

  key_algorithm   = tls_private_key.issuer_key[0].algorithm
  private_key_pem = tls_private_key.issuer_key[0].private_key_pem

  subject {
    common_name = "identity.linkerd.cluster.local"
  }
}

# issuer cert , signed by CA
resource "tls_locally_signed_cert" "issuer_cert" {
  count = var.enable_linkerd ? 1 : 0

  cert_request_pem      = tls_cert_request.issuer_req[0].cert_request_pem
  ca_key_algorithm      = tls_private_key.trustanchors_key[0].algorithm
  ca_private_key_pem    = tls_private_key.trustanchors_key[0].private_key_pem
  ca_cert_pem           = tls_self_signed_cert.trustanchors_cert[0].cert_pem
  validity_period_hours = 8760
  is_ca_certificate     = true

  allowed_uses = [
    "crl_signing",
    "cert_signing",
    "server_auth",
    "client_auth"
  ]
}

resource "helm_release" "linkerd" {
  count = var.enable_linkerd ? 1 : 0

  name       = "linkerd"
  repository = "https://helm.linkerd.io/stable"
  chart      = "linkerd2 "
  version    = "2.8.1"

  set {
    name  = "global.identityTrustAnchorsPEM"
    value = tls_self_signed_cert.trustanchors_cert[0].cert_pem
  }

  set {
    name  = "identity.issuer.crtExpiry"
    value = tls_locally_signed_cert.issuer_cert[0].validity_end_time
  }

  set {
    name  = "identity.issuer.tls.crtPEM"
    value = tls_locally_signed_cert.issuer_cert[0].cert_pem
  }

  set {
    name  = "identity.issuer.tls.keyPEM"
    value = tls_private_key.issuer_key[0].private_key_pem
  }

  set {
    name  = "identity.issuer.crtExpiry"
    value = timeadd("2017-11-22T00:00:00Z", "8760h")
  }

}