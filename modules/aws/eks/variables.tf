variable "cluster_name" {
  description = "Name of the main vpc"
  default     = "main"
}

variable "env" {
  description = "Environment identifier to label the resources using env key"
  default     = "default"
}

variable "cidr_block" {
  description = "CIDR block for the VPC"
  default     = "0.0.0.0/0"
}

variable "vpc_id" {
  description = "VPC id already created"
}

variable "cluster_subnets" {
  description = "Subnet where the cluster will be located"
}

variable "worker_sec_grp" {
  description = "Addition securitu group for workers"
}
