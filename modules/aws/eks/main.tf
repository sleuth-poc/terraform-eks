module "eks" {
  source           = "terraform-aws-modules/eks/aws"
  cluster_name     = var.cluster_name
  subnets          = var.cluster_subnets
  vpc_id           = var.vpc_id
  manage_aws_auth  = false
  write_kubeconfig = false
  cluster_version  = "1.17"
  enable_irsa      = true

  node_groups = {
    default = {
      desired_capacity          = 1
      max_capacity              = 3
      min_capacity              = 1
      key_name                  = ""
      source_security_group_ids = [var.worker_sec_grp]

      instance_type = "t2.small"
      k8s_labels = {
        Environment = var.env
      }
      additional_tags = {
        "kubernetes.io/cluster/${var.cluster_name}"     = "shared"
        "k8s.io/cluster-autoscaler/enabled"             = "true"
        "k8s.io/cluster-autoscaler/${var.cluster_name}" = "owned"
      }
    }
  }

  tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "Env"                                       = var.env
  }

}