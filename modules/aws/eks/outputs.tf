output "cluster_eks" {
  description = "Endpoint for EKS control plane."
  value       = module.eks
}

