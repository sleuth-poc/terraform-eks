output "worker_security_group_id" {
  description = "Security group of workers"
  value       = aws_security_group.worker_grp_one.id
}