resource "aws_security_group" "worker_grp_one" {
  name   = "${var.cluster_name}-worker-grp-one"
  vpc_id = var.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      var.cidr_block,
    ]
  }
}