data "aws_availability_zones" "available" {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.6.0"

  name                 = var.cluster_name
  cidr                 = var.cidr_block
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = [cidrsubnet(var.cidr_block, 8, 2), cidrsubnet(var.cidr_block, 8, 12), cidrsubnet(var.cidr_block, 8, 22)]
  public_subnets       = [cidrsubnet(var.cidr_block, 8, 1), cidrsubnet(var.cidr_block, 8, 11), cidrsubnet(var.cidr_block, 8, 21)]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "Env"                                       = var.env
  }

  public_subnet_tags = {
    # for ALB
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = "1"
    "Env"                                       = var.env
  }

  private_subnet_tags = {
    # for ALB
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = "1"
    "Env"                                       = var.env
  }
}
