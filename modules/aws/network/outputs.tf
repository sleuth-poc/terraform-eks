output "vpc_id" {
  description = " Id of the created VPC"
  value       = module.vpc.vpc_id
}

output "private_subnets" {
  description = " Private subnet created"
  value       = module.vpc.private_subnets
}