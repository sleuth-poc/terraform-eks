variable "oidc_url" {
  description = "OIDC URL for cluster autoscaler"
}

variable "cluster_id" {
  description = "EKS cluster id"
}